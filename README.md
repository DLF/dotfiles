My Dot Files
============

This repo contains some of my personal dotfiles.
I keep some more in a private repo.

Two configs which might be of interest for others
are my [Awesome WM](https://awesomewm.org/) configs
and my Xmodmap with ShiftLock used for an additional layer.

Theming and my Awesome WM config
================================

For Awesome WM and a few other applications 
(kitty, dmenu, powerline-shell and zathura),
I use a self-made theming scrip, so they share
a common color scheme. The concept is not the
fanciest but does its job.

However, if somebody wants to just copy my Awesome
config, the theme script had to be used. Pyratemp
and Inkscape have to be installed, and my scrips
[`recuratemp`](https://gitlab.com/DLF/scripts/-/blob/master/recuratemp)
and
[`svg2png`](https://gitlab.com/DLF/scripts/-/blob/master/svg2png)
must be in the path.

Then, go into the `build` directory and call `./set_theme <theme>` 
with `<theme>` being one of the theme files in the `themes` directory
but without the suffix. This creates additional config files based
on the *.template files.

Some themes might be broken. I just maintain them as I need them.
