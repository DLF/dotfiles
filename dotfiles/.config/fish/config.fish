alias l='lsd -alF'
alias t='lsd --tree'
alias s='git status'
alias vim=nvim
alias cds='cd ~/sandbox'
alias cdm='cd /run/media/daniel'
alias cdc='cd ~/.config/'
alias cdw='cd ~/sandbox/wiki'
alias .='cd ..'
alias ..='cd ../..'
alias ...='cd ../../..'
alias ....='cd ../../../..'
alias tm="test -d tmsu && test -d .tmsu && tmsu mount tmsu"
alias v="nvim"
alias r="ranger"
alias j="~/.local/bin/joshuto"
alias q="qalc"
alias x="tmux"
alias c="bat"
alias m="batman"
alias z="zellij"

set -x EDITOR nvim

starship init fish | source
thefuck --alias | source
zoxide init fish --cmd f | source

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -f /home/daniel/bin/anaconda3/bin/conda
    eval /home/daniel/bin/anaconda3/bin/conda "shell.fish" "hook" $argv | source
end
# <<< conda initialize <<<

