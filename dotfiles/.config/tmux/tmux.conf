#######################################################################
### List of plugins
#######################################################################
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator' # Also add Ctrl + hjkl for changing panes
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'catppuccin/tmux'
set -g @plugin 'omerxx/tmux-sessionx'


#######################################################################
### Base config
#######################################################################

# Enable mouse
set -g mouse on

# Start enumeration at 1
set -g base-index 1
set -g pane-base-index 1
set-window-option -g pane-base-index 1
set-option -g renumber-windows on

# Use VIM-like keys
set-window-option -g mode-keys vi

# Status bar on top
set-option -g status-position top


#######################################################################
### Key-Mapping
#######################################################################

# Prefix key is Ctrl-s
unbind C-b
set -g prefix C-s
bind C-s send-prefix

# Cycling windows
bind -n M-H previous-window  # Shift + Alt + H
bind -n M-L next-window  # Shift + Alt + L

# Yanking
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel

# Open panes in CWD
bind C-k split-window -v -c "#{pane_current_path}"
bind C-j split-window -h -c "#{pane_current_path}"


# BTW: Cycling panes is possible by Ctrl+h/j/k/l due to vim-tmux-nav plugin


#######################################################################
### Loading plugins
### Keep this line at the very bottom of tmux.conf
#######################################################################
run '~/.tmux/plugins/tpm/tpm'
