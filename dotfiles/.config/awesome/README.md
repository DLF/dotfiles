## powerline bar additional used colors

beautiful.taglist_bg_empty = "#888800"
beautiful.taglist_bg_occupied = "#008888"
beautiful.bg_systray = "#008888"
beautiful.tasklist_fg_normal
beautiful.tasklist_bg_normal
beautiful.tasklist_fg_focus
beautiful.tasklist_bg_focus
beautiful.tasklist_fg_urgent
beautiful.tasklist_bg_urgent
beautiful.tasklist_fg_minimize
beautiful.tasklist_bg_minimize


### self-defined
beautiful.bg_winkiller
beautiful.fg_winkiller
beautiful.bg_layoutbox
beautiful.fg_layoutbox
beautiful.bg_mainmenu
beautiful.fg_mainmenu
beautiful.bg_clock
beautiful.fg_clock
