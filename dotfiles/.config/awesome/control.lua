local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local naughty = require("naughty")

local module = {}

function module.get_globalkeys(update_gaps, bling)
    return awful.util.table.join(
        awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
                {description="show help", group="awesome"}),
        awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
                {description = "view previous", group = "tag"}),
        awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
                {description = "view next", group = "tag"}),
        awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
                {description = "go back", group = "tag"}),

        awful.key({ modkey,           }, "j",
            function ()
                awful.client.focus.byidx( 1)
            end,
            {description = "focus next by index", group = "client"}
        ),
        awful.key({ modkey,           }, "k",
            function ()
                awful.client.focus.byidx(-1)
            end,
            {description = "focus previous by index", group = "client"}
        ),
                
        -- Multimedia
        awful.key({ }, "XF86AudioRaiseVolume",    function () awful.util.spawn("amixer -D pulse sset Master 2%+") end),
        awful.key({ }, "XF86AudioLowerVolume",    function () awful.util.spawn("amixer -D pulse sset Master 2%-") end),
        awful.key({ }, "XF86AudioMute",           function () awful.util.spawn("pactl set-sink-mute 0 toggle") end),

        -- xrand menu
        awful.key({ modkey }, "^",    function () awful.spawn("bash -c \"dmenuex -H $HOME/.xrandr.dmenuex\"") end,
        {description="Open xrandr menu", group="environment"}),

        -- audio menu
        awful.key({ modkey }, "a",    function () awful.spawn("bash -c \"dmenuex -H $HOME/.audio.dmenuex\"") end,
        {description="Open audio menu", group="environment"}),

        -- emoji menu
        awful.key({ modkey }, "e",    function () awful.spawn("dmenu_emoji") end,
        {description="Open emoji menu", group="environment"}),

        -- clipboard management (needs xcmenu, dmenu and xsel)
        awful.key({ modkey }, "c", function() awful.spawn("reclip") end,
        {description="Open clipboard history (reclip)", group="environment"}),

        -- password selection
        awful.key({ modkey }, "p", function() awful.spawn("getpass") end,
        {description="Select password (getpass)", group="environment"}),

        -- udiskie unmount
        awful.key({ modkey }, "u", function() awful.spawn("dmenu_unmount") end,
        {description="Unmount a device", group="environment"}),
        
        -- 
        awful.key({ modkey }, ".", function() update_gaps("+") end,
        {description="Increase gap", group="layout"}),
        -- 
        awful.key({ modkey }, ",", function() update_gaps("-") end,
        {description="Decrease gap", group="layout"}),


-- for Amarok
--             -- awful.key({ }, "XF86AudioPlay",           function () awful.util.spawn("qdbus org.kde.amarok /Player org.freedesktop.MediaPlayer.PlayPause") end), -- for whatever reason, this is not needed
--         awful.key({ "Control" }, "XF86AudioRaiseVolume", function () awful.util.spawn("qdbus org.kde.amarok /Player org.freedesktop.MediaPlayer.VolumeUp 3") end),
--         awful.key({ "Control" }, "XF86AudioLowerVolume", function () awful.util.spawn("qdbus org.kde.amarok /Player org.freedesktop.MediaPlayer.VolumeDown 3") end),
--             -- see also: http://krischer.org/wiki/linux/shorties/dbus/amarok
--         awful.key({ }, "XF86AudioNext", function () awful.util.spawn("qdbus org.kde.amarok /Player org.freedesktop.MediaPlayer.Next") end),
--         awful.key({ }, "XF86AudioPrev", function () awful.util.spawn("qdbus org.kde.amarok /Player org.freedesktop.MediaPlayer.Prev") end),
-- for MOC
        awful.key({ }, "XF86AudioPlay",           function () awful.util.spawn("mocp -G") end),
        awful.key({ }, "XF86AudioNext", function () awful.util.spawn("mocp -f") end),
        awful.key({ }, "XF86AudioPrev", function () awful.util.spawn("mocp -r") end),
        awful.key({ "Control" }, "XF86AudioNext", function () awful.util.spawn("mocp -k +10") end),
        awful.key({ "Control" }, "XF86AudioPrev", function () awful.util.spawn("mocp -k -10") end),
        
            
-- Layout manipulation
        awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
                {description = "swap with next client by index", group = "client"}),
        awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
                {description = "swap with previous client by index", group = "client"}),
        awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
                {description = "focus the next screen", group = "screen"}),
        awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
                {description = "focus the previous screen", group = "screen"}),
        awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
                {description = "jump to urgent client", group = "client"}),
        awful.key({ modkey,           }, "Tab",
            function ()
                awful.client.focus.history.previous()
                if client.focus then
                    client.focus:raise()
                end
            end,
            {description = "go back", group = "client"}),

-- Standard program
        awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
                {description = "open a terminal", group = "launcher"}),
        awful.key({ }, "Print", function () awful.spawn("/usr/bin/spectacle") end,
                {description = "open Spectacle", group = "launcher"}),
        awful.key({ modkey,           }, "b", function () awful.spawn("firefox") end,
                {description = "open Firefox", group = "launcher"}),
        awful.key({ modkey,           }, "w", function () awful.spawn("qutebrowser") end,
                {description = "open Qutebrowser", group = "launcher"}),
        awful.key({ modkey,           }, "y", function () awful.spawn("xfce4-appfinder") end,
            {description = "open xfce4-appfinder", group = "launcher"}),

-- Basic awesome control
        awful.key({ modkey, "Control" }, "r", awesome.restart,
                {description = "reload awesome", group = "awesome"}),
        awful.key({ modkey, "Shift"   }, "q", awesome.quit,
                {description = "quit awesome", group = "awesome"}),

-- Basic layout parameter change 
        awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
                {description = "decrease master width factor", group = "layout"}),
        awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
                {description = "increase master width factor", group = "layout"}),
        awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
                {description = "increase the number of master clients", group = "layout"}),
        awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
                {description = "decrease the number of master clients", group = "layout"}),
        awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
                {description = "increase the number of columns", group = "layout"}),
        awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
                {description = "decrease the number of columns", group = "layout"}),

-- Changing layout
        awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
                {description = "select next", group = "layout"}),
        awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
                {description = "select previous", group = "layout"}),
        awful.key({ modkey,           }, "+", function () awful.layout.set(awful.layout.suit.max) end,
                {description = "select max layout", group = "layout"}),
        awful.key({ modkey,           }, "#", function () awful.layout.set(awful.layout.suit.tile) end,
                {description = "select tile layout", group = "layout"}),
        awful.key({ modkey,           }, "-", function () awful.layout.set(bling.layout.centered) end,
                {description = "select centered layout", group = "layout"}),

        awful.key({ modkey, "Control" }, "n",
                function ()
                    local c = awful.client.restore()
                    -- Focus restored client
                    if c then
                        client.focus = c
                        c:raise()
                    end
                end,
                {description = "restore minimized", group = "client"}),

-- Prompt
        --awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
        --        {description = "run prompt", group = "launcher"}),
        awful.key({ modkey },            "r",     function () awful.util.spawn("rofi -show run") end,
                {description = "run dmenu prompt", group = "launcher"}),

        awful.key({ modkey }, "x",
                function ()
                    awful.prompt.run {
                        prompt       = "Run Lua code: ",
                        textbox      = awful.screen.focused().mypromptbox.widget,
                        exe_callback = awful.util.eval,
                        history_path = awful.util.get_cache_dir() .. "/history_eval"
                    }
                end,
                {description = "lua execute prompt", group = "awesome"}),

-- Tabbing 
        awful.key({ modkey, "Control" }, ",",     function () bling.module.tabbed.pick_with_dmenu()  end,
                {description = "pick client", group = "tab group"}),
        awful.key({ modkey, "Control" }, ".",     function () bling.module.tabbed.pop()  end,
                {description = "pop client", group = "tab group"}),
        awful.key({ modkey, "Control" }, "m",     function () bling.module.tabbed.iter()  end,
                {description = "iterarte group", group = "tab group"}),

-- Others
	    awful.key( {modkey}, "q", function () awful.util.spawn("/home/daniel/sandbox/smallones/bash/other/kmadl") end)
    )
end

function module.get_clientkeys()
    return awful.util.table.join(
        awful.key({ modkey,           }, "f",
            function (c)
                c.fullscreen = not c.fullscreen
                c:raise()
            end,
            {description = "toggle fullscreen", group = "client"}),
        awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
                {description = "close", group = "client"}),
        awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
                {description = "toggle floating", group = "client"}),
        awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
                {description = "move to master", group = "client"}),
        awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
                {description = "move to screen", group = "client"}),
        awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
                {description = "toggle keep on top", group = "client"}),
        awful.key({ modkey,           }, "n",
            function (c)
                -- The client currently has the input focus, so it cannot be
                -- minimized, since minimized clients can't have the focus.
                c.minimized = true
            end ,
            {description = "minimize", group = "client"}),
        awful.key({ modkey,           }, "m",
            function (c)
                c.maximized = not c.maximized
                c:raise()
            end ,
            {description = "maximize", group = "client"})
    )
end

    
return module
