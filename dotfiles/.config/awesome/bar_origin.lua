local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")


-- the volume widget
local poor_volume_widget = require("poor_volume_widget")()


-- A text widget as launcher for the main menu
local textlauncher = wibox.widget {
    text = " ◆ ",
    font = beautiful.font,
    align  = 'left',
    widget = wibox.widget.textbox,
    menu = mymainmenu
}


-- A text widget as window closer
local windowkiller = wibox.widget {
    text = " ✚ ",
    font = beautiful.font,
    align  = 'left',
    widget = wibox.widget.textbox,
    menu = mymainmenu
}
local windowkiller_pressed = function(lx, ly, button, mods, find_widgets_result)
    local c = client.focus
    if c then
        c:kill()
    end
end
windowkiller:connect_signal("button::press", windowkiller_pressed)


-- Create a textclock widget
local mytextclock = wibox.widget.textclock()


function bar_for_screen(s, menu, taglist_buttons, tasklist_buttons)
    local text_pressed = function(lx, ly, button, mods, find_widgets_result)
        menu:show()
    end
    textlauncher:connect_signal("button::press", text_pressed)
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
        awful.button({ }, 1, function () awful.layout.inc( 1) end),
        awful.button({ }, 3, function () awful.layout.inc(-1) end),
        awful.button({ }, 4, function () awful.layout.inc( 1) end),
        awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --            mylauncher,
            textlauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            poor_volume_widget,
            -- mykeyboardlayout,
            wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
            windowkiller
        },
    }
end


return bar_for_screen
