-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget

-- extras
-- local freedesktop = require("freedesktop")


-- customization
local control = require("control")
-- require("tags")

-- Load Debian menu entries
-- require("debian.menu")

-- get the users home dir
local homedir = os.getenv ( "HOME" )

-- {{{
-- ##################################################################################################################
-- Check if this awesome instance is the only one or if there are more (we assume an instance running in Xephyr then)
-- ##################################################################################################################
        
-- -- Eeehhh.... for whatever reason, that doesn't work at the moment
local awesome_is_root = true
-- function set_awesome_is_root_true()
--     naughty.notify({title="CALLED", text="CALLED"})
--     awesome_is_root = true
-- end
-- awful.spawn.easy_async("bash -c 'ps aux | grep awesome | wc -l'", function(stdout, stderr, reason, exit_code)
--     value = tonumber(stdout)
--     if (value < 8) then                
--         set_awesome_is_root_true()
--     else
--         naughty.notify({title="No Autostart", text=string.format("To many awesome processes found (%d).",tostring(value))})
--     end
-- end)
-- 
-- if (awesome_is_root) then
--     naughty.notify({title="TRUE", text="TRUE"})
-- end
-- }}}

-- {{{
-- ##################################################################################################################
-- System Initialization
-- ##################################################################################################################
        
-- Configure displays
awful.spawn("xrandr --output HDMI2 --left-of HDMI1")
-- start composite manager (for transparency)
awful.spawn("compton")
-- configure Wacom tablet
if (awesome_is_root) then
    awful.spawn("xsetwacom set \"Wacom Bamboo Craft Finger touch\" Touch off")
    awful.spawn("xsetwacom set \"Wacom Bamboo Craft Pen stylus\" MapToOutput HDMI1")
    awful.spawn("xsetwacom set \"Wacom Bamboo Craft Pen eraser\" MapToOutput HDMI1")
end
-- }}}

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}


-- Themes define colours, icons, font and wallpapers.
beautiful.init(homedir .. "/.config/awesome/themes/athene.lua")
beautiful.taglist_bg_empty = "#888800"
beautiful.taglist_bg_occupied = "#008888"
beautiful.tasklist_bg_focus = "#880088"
beautiful.tasklist_bg_normal = "#1d1d5dbb"
beautiful.bg_systray = "#008888"
beautiful.bg_clock = "#333388"
beautiful.bg_winkiller = "#883333"
beautiful.bg_mainmenu = "#333388"

-- get an instance of the poor volume widget
local poor_volume_widget = require("poor_volume_widget")(gears.shape.powerline, {0,0,0,0}, 0)

-- {{{ Variable definitions
-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "joe"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    -- awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu

myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

-- mymainmenu = freedesktop.menu.build({
--     before = {
--         { "Awesome", myawesomemenu, beautiful.awesome_icon },
--         -- other triads can be put here
--     },
--     after = {
--         { "Open terminal", terminal },
--         -- other triads can be put here
--     }
-- })

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
--                                    { "Debian", debian.menu.Debian_menu.Debian },
                                    { "open terminal", terminal }
                                  }
                        })

-- A text widget as window closer
windowkiller = wibox.widget {
    {
        {
            text = " ✚ ",
            font = beautiful.font,
            align  = 'left',
            widget = wibox.widget.textbox,
            menu = mymainmenu
        },
        left   = 5,
        right  = 1,
        top    = 1,
        bottom = 1,
        widget = wibox.container.margin
    },
    bg = beautiful.bg_winkiller,
    widget = wibox.container.background,
    shape = function(cr, width, height)
        gears.shape.rectangular_tag(cr, width, height, -height/2)
    end,
}

local main_menue_pressed = function(lx, ly, button, mods, find_widgets_result)
    mymainmenu:show()
end


local windowkiller_pressed = function(lx, ly, button, mods, find_widgets_result)
    local c = client.focus
    if c then
        c:kill()
    end 
end
windowkiller:connect_signal("button::press", windowkiller_pressed)


-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
-- mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar

-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- Tag names in 2 dim array ([screen]/[tag-name])
tags = {
    {"1","2","3","4","5","6","7","8",},
    {"1","2","3","4","5","6","7","8",},
}


awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- get and set tag names
    if #tags < s.index then
        mytags = tags[#tags]
    else
        mytags = tags[s.index]
    end
    -- naughty.notify({title=string.format("%d", s.index), text=mytags[0]})
    awful.tag(mytags, s, awful.layout.suit.tile)

    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    s.mylayoutbox = wibox.widget {
        {
            {
                s.mylayoutbox,
                layout = wibox.layout.stack
            },
            left   = 15,
            right  = 15,
            top    = 1,
            bottom = 1,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_systray,
        widget             = wibox.container.background,
        shape = gears.shape.powerline
    }
    -- Create a taglist widget
    -- s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        style   = {
            shape = gears.shape.powerline
        },
--        layout   = {
--            spacing = -6,
--            layout  = wibox.layout.fixed.horizontal
--        },
        widget_template = {
            {
                {
                    {
                        id     = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left  = 16,
                right = 16,
                widget = wibox.container.margin
            },
            id     = 'background_role',
            widget = wibox.container.background,
        },
        buttons = taglist_buttons
    }

    -- Create a task list
    s.mytasklist = awful.widget.tasklist {
        screen   = s,
        filter   = awful.widget.tasklist.filter.currenttags,
        buttons  = tasklist_buttons,
        style    = {
            --shape_border_width = 1,
            --shape_border_color = '#777777',
            shape  = gears.shape.powerline,
        },
        layout   = {
            spacing = 0,
            layout  = wibox.layout.flex.horizontal
        },
        -- Notice that there is *NO* wibox.wibox prefix, it is a template,
        -- not a widget instance.
        widget_template = {
            {
                {
                    {
                        {
                            id     = 'icon_role',
                            widget = wibox.widget.imagebox,
                        },
                        margins = 2,
                        widget  = wibox.container.margin,
                    },
                    {
                        id     = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left  = 10,
                right = 10,
                widget = wibox.container.margin
            },
            id     = 'background_role',
            widget = wibox.container.background,
        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, bg = beautiful.bg_normal .. "55"})

    local main_menu = wibox.widget {
        {
            {
                {
                    {
                        {
                            text   = 'A',
                            widget = wibox.widget.textbox
                        },
                        widget = wibox.container.mirror,
                        reflection = {horizontal = true}
                    },
                    left   = 10,
                    right  = 10,
                    top    = 3,
                    bottom = 3,
                    widget = wibox.container.margin
                },
                shape              = gears.shape.rectangular_tag,
                bg                 = beautiful.bg_mainmenu,
                shape_border_color = beautiful.border_color,
                shape_border_width = beautiful.border_width,
                widget             = wibox.container.background,
            },
            widget = wibox.container.mirror,
            reflection = {horizontal = true }
        },
        left   = 0,
        right  = 0,
        top    = 0,
        bottom = 0,
        widget = wibox.container.margin
    }
    main_menu:connect_signal("button::press", main_menue_pressed)


    local systray = wibox.widget {
        {
            {
                wibox.widget.systray(),
                layout = wibox.layout.stack
            },
            left   = 8,
            right  = 8,
            top    = 1,
            bottom = 1,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_systray,
        widget             = wibox.container.background,
        shape = gears.shape.powerline
    }

    local mytextclock = wibox.widget {
        {
            {
                wibox.widget.textclock(),
                layout = wibox.layout.stack
            },
            left   = 8,
            right  = 8,
            top    = 1,
            bottom = 1,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_clock,
        widget             = wibox.container.background,
        shape = gears.shape.powerline
    }



    -- add widgets to the wibox
    local margin = 3
    s.mywibox:setup {
        {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                main_menu,
                s.mytaglist,
            },
            s.mytasklist, -- Middle widget
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                poor_volume_widget,
                -- mykeyboardlayout,
                systray,
                mytextclock,
                s.mylayoutbox,
                windowkiller
            },
        },
        left   = margin,
        right  = margin,
        top    = margin,
        bottom = margin,
        widget = wibox.container.margin
    }
end)

-- ##################################################################################################################
-- change tag names dynamically
-- ##################################################################################################################
-- -- disabled by now
-- -- the function added the number of clients to each tag name (not fully robust when changing a clients tag set)
-- dynamic_tagging = function() 
--     for s = 1, screen.count() do
--         local atags = screen[s].tags
--         for i, t in ipairs(atags) do
--             local client_number = (#(t:clients()))
--             local name = t.name
--             local start_of_indicator = string.find(name, " %[%d+%]")
--             if start_of_indicator then
--                 name = string.sub(name, 1, start_of_indicator - 1)
--             end
--             if client_number > 0 then
--                 name = name .. " [" .. client_number .. "]"
--             end
--             t.name = name
--         end
--     end
-- end 
-- -- signal function to execute when a new client appears
-- client.connect_signal("manage", function(c)
--     dynamic_tagging()
-- end)
-- -- signal function to execute when a client disappears
-- client.connect_signal("unmanage", function (c)
--     dynamic_tagging()
-- end)

-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = control.get_globalkeys()

clientkeys = control.get_clientkeys()

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { 
        rule_any = {
            type = {"dialog"}
        },
        properties = {
            titlebars_enabled = true
        }
    },
    { 
        rule_any = {
            type = {"normal"}
        },
        properties = {
            titlebars_enabled = true
        }
    },
--    {   rule_any = {
--            instance = { "urxvt", "terminology", "vivaldi-bin", "vivaldi", "vivaldi-stable", "kitty"},
--            class = {'terminology', "vivaldi-stable", "qutebrowser", "zathura", "Zathura"}
--        },
--        properties = {
--            titlebars_enabled = false
--        },
--        --callback = titlebar_add_with_settings
--     },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

set_gap = function(t) 
    if t.layout == awful.layout.suit.tile then
        t.gap = 3
    else 
        t.gap = 0
    end
end

set_titlebar_according_to_floating = function(c)
    if c ~= nil then
        if c.floating then
            awful.titlebar.show(c)
        else
            awful.titlebar.hide(c)
        end
    end
end

-- selective titlebars (only for floatings) inspired by http://www.holgerschurig.de/en/awesome-4.0-titlebars/
-- Signla when a clients floating status changes
client.connect_signal("property::floating", function (c)
    set_titlebar_according_to_floating(c)
end)


-- Signal when a screen is layouted
tag.connect_signal("property::layout", function(t)
    set_gap(t)
    if t.layout.name == "floating" then
        for _,c in pairs(t:clients()) do
            awful.titlebar.hide(c)
        end
    else
        set_titlebar_according_to_floating(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c, {size=18}) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
    -- Hide the menubar if we are not floating
    local l = awful.layout.get(c.screen)
    if not (l.name == "floating" or c.floating) then
        awful.titlebar.hide(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c)
    c.border_color = beautiful.border_focus
    c.opacity = 1
end)

client.connect_signal("unfocus", function(c)
    c.border_color = beautiful.border_normal
    -- full opacity for some programs, even if they don't have the focus
    if (
        c.instance == "vivaldi" or
        c.instance == "vivaldi-stable" or
        c.instance == "vivaldi-bin" or
        c.instance == "smplayer" or
        c.instance == "firefox" or
        c.instance == "Firefox" or
        c.instance == "chromium" or
        c.instance == "gwenview" or
        c.instance == "geeqie" or
        c.instance == "openscad" or
        c.class == "OpenSCAD"
    ) then
        c.opacity = 1
    else
        c.opacity = 0.80
    end
end)

-- }}}

-- iterate over all tags and call set_gap for each
for s = 1, screen.count() do
    local atags = screen[s].tags
    for i, t in ipairs(atags) do
        set_gap(t)
    end
end


-- autostart of apps
if (awesome_is_root) then
    --awful.spawn("/usr/bin/parcellite --no-icon")
    awful.spawn("xcmenu --daemon")
    awful.spawn("udiskie -t")

--     awful.spawn("/usr/bin/vivaldi", true, function(c)
--         c:move_to_tag(awful.tag.find_by_name(nil, "WWW"))
--         end)
    awful.spawn("kitty", true, function(c)
       c:move_to_tag(awful.tag.find_by_name(nil, tags[0][0]))
    end)
--     awful.spawn("urxvt -e mocp", true, function(c)
--         c:move_to_tag(awful.tag.find_by_name(nil, "Dash"))
--         end)
--     awful.spawn("urxvt -e htop", true, function(c)
--         c:move_to_tag(awful.tag.find_by_name(nil, "Dash"))
--         end)
--     awful.spawn("urxvt -e profanity", true, function(c)
--         c:move_to_tag(awful.tag.find_by_name(nil, "Communication"))
--         end)
--     awful.spawn("urxvt -e calcurse", true, function(c)
--         c:move_to_tag(awful.tag.find_by_name(nil, "Communication"))
--         end)
--     awful.spawn("urxvt -e newsboat", true, function(c)
--         c:move_to_tag(awful.tag.find_by_name(nil, "Communication"))
--         end)
end

