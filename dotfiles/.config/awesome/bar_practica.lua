local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

beautiful.taglist_bg_empty = beautiful.taglist_bg_empty or beautiful.bg_minimize
beautiful.taglist_bg_occupied = beautiful.taglist_bg_occupied or beautiful.bg_normal
beautiful.taglist_bg_focus = beautiful.taglist_bg_focus or beautiful.bg_focus
beautiful.taglist_fg_empty = beautiful.taglist_fg_empty or beautiful.fg_minimize
beautiful.taglist_fg_occupied = beautiful.taglist_fg_occupied or beautiful.fg_normal
beautiful.taglist_fg_focus = beautiful.taglist_fg_focus or beautiful.fg_focus
beautiful.tasklist_bg_focus = beautiful.tasklist_bg_focus or beautiful.bg_focus
beautiful.tasklist_bg_normal = beautiful.tasklist_bg_normal or beautiful.bg_normal
beautiful.tasklist_fg_focus = beautiful.tasklist_fg_focus or beautiful.fg_focus
beautiful.tasklist_fg_normal = beautiful.tasklist_fg_normal or beautiful.fg_normal


-- A text widget as window closer
windowkiller = wibox.widget {
    {
        {
            text = " ⨯ ",
            font = beautiful.font,
            align  = 'left',
            widget = wibox.widget.textbox,
        },
        left   = 4,
        right  = 2,
        top    = 0,
        bottom = 0,
        widget = wibox.container.margin
    },
    bg = beautiful.bg_winkiller or beautiful.bg_normal,
    fg = beautiful.fg_winkiller or beautiful.fg_normal,
    widget = wibox.container.background,
    shape = gears.shape.rectangle
}


local windowkiller_pressed = function(lx, ly, button, mods, find_widgets_result)
    awful.spawn("xkill")
end
windowkiller:connect_signal("button::press", windowkiller_pressed)

local poor_volume_widget = require("poor_volume_widget")(
    gears.shape.rectangle,
    gears.shape.rectangle,
    {3,5,3,1},
    0
)


function bar_for_screen(s, menu, taglist_buttons, tasklist_buttons)
    local main_menu_pressed = function(lx, ly, button, mods, find_widgets_result)
        menu:show()
    end
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
        awful.button({ }, 1, function () awful.layout.inc( 1) end),
        awful.button({ }, 3, function () awful.layout.inc(-1) end),
        awful.button({ }, 4, function () awful.layout.inc( 1) end),
        awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    s.mylayoutbox = wibox.widget {
        {
            {
                s.mylayoutbox,
                layout = wibox.layout.stack
            },
            left   = 3,
            right  = 3,
            top    = 0,
            bottom = 0,
            widget = wibox.container.margin
        },
        bg = beautiful.bg_layoutbox or beautiful.bg_normal,
        fg = beautiful.fg_layoutbox or beautiful.fg_normal,
        widget = wibox.container.background,
        shape = gears.shape.rectangle
    }

    -- Create a taglist widget
    -- s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        style   = {
            shape = gears.shape.rectangle
            -- shape = gears.shape.parallelogram
        },
        widget_template = {
            {
                {
                    {
                        {
                            {
                                id = 'text_role',
                                widget = wibox.widget.textbox,
                            },
                            layout = wibox.layout.fixed.horizontal,
                        },
                        left  = 6,
                        right = 6,
                        top = 0,
                        widget = wibox.container.margin
                    },
                    id     = 'background_role',
                    widget = wibox.container.background,
                },
                left  = 2,
                right = 2,
                widget = wibox.container.margin
            },
            widget = wibox.container.background,
            bg = beautiful.bg_normal,
        },
        buttons = taglist_buttons
    }

    -- Create a task list
    s.mytasklist = awful.widget.tasklist {
        screen   = s,
        filter   = awful.widget.tasklist.filter.currenttags,
        buttons  = tasklist_buttons,
        style    = {
            shape = gears.shape.rectangle
        },
        layout   = {
            spacing = 0,
            layout  = wibox.layout.flex.horizontal,
            max_widget_size = 300
        },
        -- Notice that there is *NO* wibox.wibox prefix, it is a template,
        -- not a widget instance.
        widget_template = {
            {
                {
                    {
                        {
                            id     = 'icon_role',
                            widget = wibox.widget.imagebox,
                            resize = true
                        },
                        left = 3,
                        right = 3,
                        widget  = wibox.container.margin,
                    },
                    {
                        id     = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                id     = 'background_role',
                widget = wibox.container.background,
            },
            left  = 2,
            right = 2,
            widget = wibox.container.margin
        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar({
        position = "top",
        screen = s,
        bg = beautiful.bg_normal .. "A0",
        fg = beautiful.fg_normal .. "0",
        height = 18
    })

    local systray = wibox.widget {
        {
            {
                wibox.widget.systray(),
                layout = wibox.layout.stack
            },
            left   = 2,
            right  = 2,
            top    = 0,
            bottom = 0,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_systray or  beautiful.bg_normal,
        widget             = wibox.container.background,
        shape = gears.shape.rectangle
    }

    local mytextclock = wibox.widget {
        {
            {
                wibox.widget.textclock(),
                layout = wibox.layout.stack
            },
            left   = 2,
            right  = 2,
            top    = 0,
            bottom = 0,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_clock or beautiful.bg_normal,
        fg                 = beautiful.fg_clock or beautiful.fg_normal,
        widget             = wibox.container.background,
        shape = gears.shape.rectangle
    }



    -- add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
        },
        { -- Middle widgets
            -- another align.horizontal with EMPTY left and right to get the task buttons centered
            widget = wibox.layout.align.horizontal,
            {
                left  = 0,
                right = 0,
                widget = wibox.container.margin
            },
            {
                {
                    s.mytasklist, -- Middle widget
                    layout = wibox.layout.stack
                },
                left  = 30,
                right = 30,
                widget = wibox.container.margin
            },
            {
                left  = 0,
                right = 0,
                widget = wibox.container.margin
            },
            -- {},
        },
        { -- Right widgets
            {
                layout = wibox.layout.fixed.horizontal,
                {
                    {
                        poor_volume_widget,
                        layout = wibox.layout.stack
                    },
                    left  = 10,
                    right = 6,
                    widget = wibox.container.margin
                },
                -- mykeyboardlayout,
                systray,
                mytextclock,
                s.mylayoutbox,
                windowkiller
            },
            widget = wibox.container.background,
            bg = beautiful.bg_normal,
        },
    }
end


return bar_for_screen
