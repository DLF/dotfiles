local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

beautiful.taglist_bg_empty = beautiful.taglist_bg_empty or beautiful.bg_minimize
beautiful.taglist_bg_occupied = beautiful.taglist_bg_occupied or beautiful.bg_normal
beautiful.taglist_bg_focus = beautiful.taglist_bg_focus or beautiful.bg_focus
beautiful.taglist_fg_empty = beautiful.taglist_fg_empty or beautiful.fg_minimize
beautiful.taglist_fg_occupied = beautiful.taglist_fg_occupied or beautiful.fg_normal
beautiful.taglist_fg_focus = beautiful.taglist_fg_focus or beautiful.fg_focus
beautiful.tasklist_bg_focus = beautiful.tasklist_bg_focus or beautiful.bg_focus
beautiful.tasklist_bg_normal = beautiful.tasklist_bg_normal or beautiful.bg_normal
beautiful.tasklist_fg_focus = beautiful.tasklist_fg_focus or beautiful.fg_focus
beautiful.tasklist_fg_normal = beautiful.tasklist_fg_normal or beautiful.fg_normal


-- A text widget as window closer
windowkiller = wibox.widget {
    {
        {
            text = " 🕱 ",
            font = beautiful.font,
            align  = 'left',
            widget = wibox.widget.textbox,
        },
        left   = 4,
        right  = 2,
        top    = 1,
        bottom = 1,
        widget = wibox.container.margin
    },
    bg = beautiful.bg_winkiller or beautiful.bg_normal,
    fg = beautiful.fg_winkiller or beautiful.fg_normal,
    widget = wibox.container.background,
    shape = gears.shape.rectangle
}


local windowkiller_pressed = function(lx, ly, button, mods, find_widgets_result)
    local c = client.focus
    if c then
        c:kill()
    end
end
windowkiller:connect_signal("button::press", windowkiller_pressed)

local poor_volume_widget = require("poor_volume_widget")(
    gears.shape.rectangle,
    gears.shape.rectangle,
    {0,0,0,0},
    0
)


function bar_for_screen(s, menu, taglist_buttons, tasklist_buttons)
    local main_menu_pressed = function(lx, ly, button, mods, find_widgets_result)
        menu:show()
    end
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
        awful.button({ }, 1, function () awful.layout.inc( 1) end),
        awful.button({ }, 3, function () awful.layout.inc(-1) end),
        awful.button({ }, 4, function () awful.layout.inc( 1) end),
        awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    s.mylayoutbox = wibox.widget {
        {
            {
                s.mylayoutbox,
                layout = wibox.layout.stack
            },
            left   = 3,
            right  = 3,
            top    = 1,
            bottom = 1,
            widget = wibox.container.margin
        },
        bg = beautiful.bg_layoutbox or beautiful.bg_normal,
        fg = beautiful.fg_layoutbox or beautiful.fg_normal,
        widget = wibox.container.background,
        shape = gears.shape.rectangle
    }

    -- Create a taglist widget
    -- s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        style   = {
            shape = gears.shape.rectangle
        },
        widget_template = {
            {
                {
                    {
                        id     = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                left  = 3,
                right = 3,
                top = 3,
                widget = wibox.container.margin
            },
            id     = 'background_role',
            widget = wibox.container.background,
        },
        buttons = taglist_buttons
    }

    -- Create a task list
    s.mytasklist = awful.widget.tasklist {
        screen   = s,
        filter   = awful.widget.tasklist.filter.currenttags,
        buttons  = tasklist_buttons,
        style    = {
            shape = gears.shape.rectangle
        },
        layout   = {
            spacing = 0,
            layout  = wibox.layout.flex.horizontal
        },
        -- Notice that there is *NO* wibox.wibox prefix, it is a template,
        -- not a widget instance.
        widget_template = {
            {
                {
                    {
                        {
                            id     = 'icon_role',
                            widget = wibox.widget.imagebox,
                        },
                        margins = 3,
                        widget  = wibox.container.margin,
                    },
                    {
                        id     = 'text_role',
                        widget = wibox.widget.textbox,
                    },
                    layout = wibox.layout.fixed.horizontal,
                },
                id     = 'background_role',
                widget = wibox.container.background,
            },
            left  = 3,
            right = 3,
            widget = wibox.container.margin
        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, bg = beautiful.bg_normal .. "55"})

    local main_menu = wibox.widget {
        {
            {
                {
                    {
                        text   = '☀',
                        widget = wibox.widget.textbox,
                        font = beautiful.font_mainmenu or beautiful.font
                    },
                    widget = wibox.container.mirror,
                    reflection = {horizontal = true}
                },
                left   = 3,
                right  = 3,
                top    = 3,
                bottom = 3,
                widget = wibox.container.margin
            },
            shape              = gears.shape.rectangle,
            bg                 = beautiful.bg_mainmenu or beautiful.bg_normal,
            fg                 = beautiful.fg_mainmenu or beautiful.fg_normal,
            widget             = wibox.container.background,
        },
        left   = 0,
        right  = 0,
        top    = 0,
        bottom = 0,
        widget = wibox.container.margin
    }
    main_menu:connect_signal("button::press", main_menu_pressed)


    local systray = wibox.widget {
        {
            {
                wibox.widget.systray(),
                layout = wibox.layout.stack
            },
            left   = 2,
            right  = 2,
            top    = 1,
            bottom = 1,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_systray or  beautiful.bg_normal,
        widget             = wibox.container.background,
        shape = gears.shape.rectangle
    }

    local mytextclock = wibox.widget {
        {
            {
                wibox.widget.textclock(),
                layout = wibox.layout.stack
            },
            left   = 2,
            right  = 2,
            top    = 1,
            bottom = 1,
            widget = wibox.container.margin
        },
        bg                 = beautiful.bg_clock or beautiful.bg_normal,
        fg                 = beautiful.fg_clock or beautiful.fg_normal,
        widget             = wibox.container.background,
        shape = gears.shape.rectangle
    }



    -- add widgets to the wibox
    local margin = 3
    s.mywibox:setup {
        {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                {
                    {
                        main_menu,
                        layout = wibox.layout.stack
                    },
                    left  = 3,
                    right = 3,
                    widget = wibox.container.margin
                },
                s.mytaglist,
            },
            {
                {
                    s.mytasklist, -- Middle widget
                    layout = wibox.layout.stack
                },
                left  = 3,
                right = 3,
                widget = wibox.container.margin
            },
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                {
                    {
                        poor_volume_widget,
                        layout = wibox.layout.stack
                    },
                    left  = 4,
                    right = 6,
                    widget = wibox.container.margin
                },
                -- mykeyboardlayout,
                systray,
                mytextclock,
                s.mylayoutbox,
                windowkiller
            },
        },
        left   = margin,
        right  = margin,
        top    = 2,
        bottom = margin,
        widget = wibox.container.margin
    }
end


return bar_for_screen
