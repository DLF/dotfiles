local result = {}
result.rules = {

    { rule = { class = "firefox" },
       properties = { tag = "FF" } },

    { rule = { class = "qutebrowser" },
       properties = { tag = "QB" } },
                
}
return result
