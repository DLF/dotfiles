-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: bash
Binary: bash, bash-static, bash-builtins, bash-doc, bashdb
Architecture: any
Version: 4.2-0ubuntu3
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Homepage: http://tiswww.case.edu/php/chet/bash/bashtop.html
Standards-Version: 3.8.3
Build-Depends: autoconf, autotools-dev, patch, bison, libncurses5-dev, texinfo, debhelper (>= 5), texi2html, locales, gettext, sharutils, time, xz-utils
Build-Depends-Indep: texlive-latex-base, ghostscript
Checksums-Sha1: 
 873abc841ebbe0b4507a3d0975721e32878c971c 4266049 bash_4.2.orig.tar.gz
 c5d485abe8ee4d4ce1f0a736fe16b13d9568cb69 90997 bash_4.2-0ubuntu3.diff.gz
Checksums-Sha256: 
 4ad124cc7b8949731061598d05cf8eaae1766e65c0f2e7d9406fb410a01a04ae 4266049 bash_4.2.orig.tar.gz
 70c36711abbd6fdc81c97b7162ea238a89d2e787414440935a33d37ca2c4c474 90997 bash_4.2-0ubuntu3.diff.gz
Files: 
 50b2329633d2d60307731c23682145f6 4266049 bash_4.2.orig.tar.gz
 8b1cdee87e13824bf8e7762be308d168 90997 bash_4.2-0ubuntu3.diff.gz
Original-Maintainer: Matthias Klose <doko@debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAk2Up/EACgkQStlRaw+TLJxm5ACfWEZUgqxunV0wddufPZPSSV/a
y0AAoKpBKxTiPsalGdS0+HDwv368Ekzz
=/mVO
-----END PGP SIGNATURE-----
