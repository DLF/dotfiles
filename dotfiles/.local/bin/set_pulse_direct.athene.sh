#!/usr/bin/env bash
killall alsa_out
alsa_internal_ix=`pactl list short sinks | grep alsa_output.pci | awk '{print $1}'`
cart_internal_ix=`pactl list short cards | grep alsa_card.pci | awk '{print $1}'`
pactl set-card-profile $cart_internal_ix output:analog-stereo
sleep 1
pasetsinks $alsa_internal_ix

