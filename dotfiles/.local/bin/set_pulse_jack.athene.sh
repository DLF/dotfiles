#!/usr/bin/env bash
jack_ix=`pactl list short sinks | grep jack_out | awk '{print $1}'`
cart_internal_ix=`pactl list short cards | grep alsa_card.pci | awk '{print $1}'`
pactl set-card-profile $cart_internal_ix off
pasetsinks $jack_ix
sleep 1
alsa_out -d hw:0 &
sleep 1
jack_connect "PulseAudio JACK Sink:front-left" "alsa_out:playback_1"
sleep 1
jack_connect "PulseAudio JACK Sink:front-right" "alsa_out:playback_2"

