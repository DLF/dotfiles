#!/bin/bash

stderr=""

function error {
    if [ $1 -ne 0 ]; then
    	notify-send "Error on starting Jack."
        exit 1
    fi
}

jack_control start
error $?
jack_control ds alsa
jack_control dps device hw:PCH,0
jack_control dps rate 48000
jack_control dps nperiods 2
jack_control dps period 64
jack_control dps midi-driver seq
sleep 2
pactl load-module module-jack-sink channels=2
pactl load-module module-jack-source channels=2
pacmd set-default-sink jack_out
pacmd set-default-source jack_in
a2jmidid -e
notify-send "Jack running."

