#!/usr/bin/env bash
set -e
cd ~
mkdir -p .virtualenvs
cd .virtualenvs
python3 -m venv debugpy
debugpy/bin/python -m pip install debugpy
